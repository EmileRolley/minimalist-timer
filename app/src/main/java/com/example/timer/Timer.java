package com.example.timer;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.widget.TextView;

public class Timer {

	public static final Time DEFAULT_TIME = new Time(3, 0);

	private Time initialTime;
	private Time currentTime;

	private static class Time {

    	private int minutes;
    	private int seconds;

		public Time() {
			this(0, 0);
		}

    	public Time(int minutes, int seconds) {
    		this.minutes = minutes;
    		this.seconds = seconds;
		}

		public Time(Time time) {
			this.minutes = time.minutes;
			this.seconds = time.seconds;
		}

		public void increment(int secondsToIncrement) {
			int totalSeconds = seconds + secondsToIncrement;

			seconds = totalSeconds % 60;
			minutes = (minutes + totalSeconds / 60) % 60;
		}

		public void decrement(int secondsToDecrement) {
			int remainingSeconds = seconds - secondsToDecrement;

			seconds = (remainingSeconds < 0) ? 60 + (remainingSeconds % 60) : remainingSeconds;
			if (remainingSeconds < 0)
				minutes -= remainingSeconds / 60 + 1;
			if (minutes < 0)
				minutes = 0;
		}

		public String formatToString() {
			return formatToStringBis(minutes) + ":" + formatToStringBis(seconds);
		}

		private String formatToStringBis(int number) {
			return (number / 10 > 0) ? String.valueOf(number) : "0" + String.valueOf(number);
		}
	}

    private boolean isRunning = false;
	private boolean currentTimeInitialized = false;

    private TextView timerTextView;

    public Timer(TextView timerTextView) {
    	this.initialTime = new Time(DEFAULT_TIME);
    	this.timerTextView = timerTextView;
	}

	public void updateTimerView() {
		this.timerTextView.setText(
				currentTimeInitialized ? currentTime.formatToString() : initialTime.formatToString()
		);
	}

	public void decrementTimer(int seconds) {
    	initialTime.decrement(seconds);
	}

	public void incrementTimer(int seconds) {
		initialTime.increment(seconds);
	}

	public void play() {
    	if (!isRunning) {
			if (!currentTimeInitialized) {
				reset();
				currentTimeInitialized = true;
			}
			isRunning = true;
			timerTextView.setTextColor(Color.WHITE);
			new Thread(new Runnable() {
				@Override
				public void run() {
					while ( currentTime.minutes > 0 || currentTime.seconds > 0) {
						try {
							Thread.sleep(1000);
							if (!isRunning)
								return;
							currentTime.decrement(1);
							updateTimerView();
						} catch (InterruptedException e) {
							e.printStackTrace();
							return;
						}
					}
					reset();
					MainActivity.mediaPlayer.start();
				}
			}).start();
		}
		else {
			timerTextView.setTextColor(Color.LTGRAY);
			isRunning = false;
		}
	}

	public void reset() {
		currentTime = new Time(initialTime);
		currentTimeInitialized = false;
		isRunning = false;
//		timerTextView.setTextColor(Color.WHITE);
		updateTimerView();
	}
}
