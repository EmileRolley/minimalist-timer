package com.example.timer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static MediaPlayer mediaPlayer;

    private static final int SECONDS_STEP = 30;

    private Timer timer;



    private class OnClickTimer implements View.OnLongClickListener, View.OnClickListener {
        @Override
        public boolean onLongClick(View v) {
            Log.e("** DEBUG **","onLongClick");
            timer.reset();
            return true;
        }

        @Override
        public void onClick(View v) {
            timer.play();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = (TextView) findViewById(R.id.main_text_view);
        FrameLayout layout = findViewById(R.id.frame_layout);

        textView.setOnClickListener(new OnClickTimer());
        layout.setOnClickListener(new OnClickTimer());
        textView.setOnLongClickListener(new OnClickTimer());
        layout.setOnLongClickListener(new OnClickTimer());

        mediaPlayer = MediaPlayer.create(this, R.raw.gong);

        this.timer = new Timer(textView);
        this.timer.updateTimerView();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public void decreaseTimer(View mainView) {
        timer.decrementTimer(SECONDS_STEP);
        timer.updateTimerView();
    }

    public void increaseTimer(View mainView) {
        timer.incrementTimer(SECONDS_STEP);
        timer.updateTimerView();
    }
}
