# Minimalist Timer

A minimalist timer.

# APK

Application available here [Minimalist Timer](https://gitlab.com/EmileRolley/minimalist-timer/-/releases).

# How To

1. Use *plus* and *minus* buttons for select a time.
2. Then one *tap* will start the timer.
3. An other *tap* will pause it.
4. A *holded tap* will reset it.
